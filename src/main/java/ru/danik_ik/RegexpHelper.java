package ru.danik_ik;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class RegexpHelper {
    // TODO: 30.07.2019 вынести строки в ресурсы по языкам
    private static final Pattern groupNamePattern = Pattern.compile("\\(\\?<(?<name>\\w+)>");

    private RegexpHelper() {}

    public static String processRegexp(String regexp, String input) {
        if ("".equals(regexp)) return "Пустое выражение";
        if ("".equals(input)) return "Пустые входные данные";
        Pattern pattern;
        try {
            pattern = Pattern.compile(regexp);
        } catch (PatternSyntaxException pse) {
            return String.format("Синтаксическая ошибка в регулярном выражении: %s%n%n", pse.getMessage())
                    + String.format("Описание ошибки: %s%n%n", pse.getDescription())
                    + String.format("Позиция ошибки: %s%n%n", pse.getIndex())
                    + String.format("Ошибочный шаблон: %s%n%n", pse.getPattern())
                    ;
        }
        Matcher matcher = pattern.matcher(input);

        boolean found = false;
        StringBuilder out = new StringBuilder();
        int index = 1;
        while (matcher.find()) {
            found = true;
            out.append(String.format("Совпадение №%d: «%s»%n", index++, matcher.group(0)));
            for (int i = 1; i <= matcher.groupCount(); i++) {
                out.append(String.format("  Группа №%d: «%s»%n", i, matcher.group(i)));
            }
            Matcher groupNameMatcher = groupNamePattern.matcher(regexp);
            while (groupNameMatcher.find()) {
                final String groupName = groupNameMatcher.group("name");
                try {
                    out.append(String.format("  Группа «%s»: «%s»%n", groupName, matcher.group(groupName)));
                } catch (Exception e) {
                    out.append(String.format(
                        "  Группа «%s»: ошибка при попытке прочитать именованную группу: %s%n",
                        groupName,
                        e.getMessage()
                    ));
                }
            }
        }
        return found ? out.toString() : "Совпадения не найдены";
    }
}
