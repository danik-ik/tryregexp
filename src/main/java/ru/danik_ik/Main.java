package ru.danik_ik;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.util.function.Consumer;

class Main {
    // TODO: 30.07.2019 вынести строки в ресурсы по языкам

    JTextField regexp;
    private JTextArea inputText;
    private JTextArea outputText;

    public void addComponentsToPane(Container pane) {
        pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
        regexp = new JTextField();
        regexp.setText("Введите здесь регулярное выражение java");
        regexp.selectAll();
        regexp.getDocument().addDocumentListener(new CommonDocumentListener(this::onConditionsChanged));
        pane.add(regexp);

        inputText = new JTextArea();
        inputText.setText("Введите здесь текст, к которому будет применено регулярное выражение");
        inputText.selectAll();
        inputText.setRows(10);
        inputText.setColumns(80);
        inputText.setBorder(regexp.getBorder());
        inputText.getDocument().addDocumentListener(new CommonDocumentListener(this::onConditionsChanged));

        pane.add(new JScrollPane (inputText,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));

        outputText = new JTextArea();
        outputText.setText("Здесь Вы увидите результат");
        outputText.setFont(new Font("monospaced", Font.PLAIN, outputText.getFont().getSize()));
        outputText.setRows(40);
        outputText.setColumns(80);
        outputText.setBorder(regexp.getBorder());
        outputText.setEditable(false);
        pane.add(new JScrollPane (outputText,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));

    }

    private void onConditionsChanged(DocumentEvent event) {
        SwingUtilities.invokeLater( () -> {
            outputText.setText(RegexpHelper.processRegexp(regexp.getText(), inputText.getText()));
        });
    }

    private void createAndShowGUI() {
        // Создание фрейма
        JFrame frame = new JFrame("tryRegexp");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        addComponentsToPane(frame.getContentPane());

        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[ ] args) {
        // запустить приложение
        SwingUtilities.invokeLater( () -> new Main().createAndShowGUI() );
    }

    private class CommonDocumentListener implements DocumentListener {
        private final Consumer<DocumentEvent> handler;

        public CommonDocumentListener(Consumer<DocumentEvent> handler) {
            this.handler = handler;
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
            handler.accept(e);
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            handler.accept(e);
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            handler.accept(e);
        }
    }
}